package edu.pnw.softwaredesign.avatar;

/**
 * Creates the Shorts cosmetic.
 * 
 * @author Piyush Kochhar
 *
 */
public class Shorts extends AvatarDecorator {
  
  private String cosmetic = "Shorts";
  
  private Avatar avatar;
  
  /**
   * Adds Shorts to Avatar.
   * 
   * @param avatar Takes an Avatar Object
   */
  public Shorts(Avatar avatar) {
    super();
    this.avatar = avatar;
    Avatar.cosmetics.add(cosmetic);
  }
  
  @Override
  public String getDescription() {
    return cosmetic;
  }
  
}
