package edu.pnw.softwaredesign.avatar;

/**
 * Prints list of Gender for Avatar.
 * 
 * @author Piyush Kochhar
 *
 */
public class GenderList {
  
  /**
   * Prints Gender lsit.
   */
  public static void print() {
    
    String[] genderList = new String[] {"Male", "Female"};
    
    for (int i = 0; i < genderList.length; i++) {
      System.out.println(i + 1 + ". " + genderList[i]);
    }
    
  }
  
}
