package edu.pnw.softwaredesign.avatar;

/**
 * Prints the list of cosmetics.
 * 
 * @author Piyush Kochhar
 *
 */
public class CosmeticList {
  
  /**
   * Prints the cosmetic list.
   */
  public static void print() {
    
    String[] cosmeticList = new String[] {"Jacket", "T-shirt", "Sweater", "Jeans", "Shorts", "Tie",
        "Necklace", "Earrings", "Sunglasses", "Leather Shoes", "High Heels", "Running Shoes"};
    
    for (int i = 0; i < cosmeticList.length; i++) {
      System.out.println(i + 1 + ". " + cosmeticList[i]);
    }
    
  }
  
}
