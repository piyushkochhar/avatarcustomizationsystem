package edu.pnw.softwaredesign.avatar;

import java.util.LinkedList;

/**
 * Creates an Avatar (Component).
 * 
 * @author Piyush Kochhar
 *
 * 
 */
public abstract class Avatar {
  
  static LinkedList<String> cosmetics = new LinkedList<String>();
  
  protected String description;
  
  /**
   * Description of Avatar.
   * 
   * @return description of the avatar
   */
  public String getDescription() {
    return description;
  }
  
  public void setDescription(String description) {
    this.description = description;
  }
  
  
}
