package edu.pnw.softwaredesign.avatar;

/**
 * Creates the Tie cosmetics.
 * 
 * @author Piyush Kochhar
 *
 */
public class Tie extends AvatarDecorator {
  
  private String cosmetic = "Tie";
  
  private Avatar avatar;
  
  /**
   * Adds Tie to Avatar.
   * 
   * @param avatar Takes an Avatar Object
   */
  public Tie(Avatar avatar) {
    super();
    this.avatar = avatar;
    Avatar.cosmetics.add(cosmetic);
  }
  
  @Override
  public String getDescription() {
    return cosmetic;
  }
  
}
