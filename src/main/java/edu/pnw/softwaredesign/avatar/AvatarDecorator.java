package edu.pnw.softwaredesign.avatar;

/**
 * Decorator for the avatar.
 * 
 * @author Piyush Kochhar
 *
 */
public abstract class AvatarDecorator extends Avatar {
  public abstract String getDescription();
  
  
  
}
