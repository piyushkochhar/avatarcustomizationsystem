package edu.pnw.softwaredesign.avatar;

/**
 * Creates Necklace Cosmetic.
 * 
 * @author Piyush Kochhar
 *
 */
public class Necklace extends AvatarDecorator {
  
  private String cosmetic = "Necklace";
  
  private Avatar avatar;
  
  /**
   * Adds Necklace to Avatar.
   * 
   * @param avatar Takes an Avatar Object
   */
  public Necklace(Avatar avatar) {
    super();
    this.avatar = avatar;
    Avatar.cosmetics.add(cosmetic);
  }
  
  @Override
  public String getDescription() {
    return cosmetic;
  }
  
}
