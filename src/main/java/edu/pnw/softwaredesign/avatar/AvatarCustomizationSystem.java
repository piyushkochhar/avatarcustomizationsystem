package edu.pnw.softwaredesign.avatar;

/**
 * Functionality of the AvatarCustomization.
 * 
 * @author Piyush Kochhar
 *
 */
public class AvatarCustomizationSystem {
  
  /**
   * Runs the Avatar System.
   */
  public void run() {
    
    System.out.println("Please select your character");
    
    GenderList.print();
    
    UserInput option = new UserInput();
    
    Avatar avatar = new AvatarWithGender(option.getInput());
    
    System.out.println("You have selected the " + avatar.getDescription());
    
    while (true) {
      System.out
          .println("\nPlease select a cosmetic for your character:\n" + "(Type ‘exit’ to finish)");
      
      CosmeticList.print();
      
      option = new UserInput();
      
      if (option.getInput().equals("exit")) {
        break;
      } else if (Character.isLetter(option.getInput().charAt(0))) {
        System.out.println("Invalid Input");
        System.out.println("System exiting...");
        return;
      }
      
      AvatarWithCosmetics superAvatar = new AvatarWithCosmetics(avatar);
      
      superAvatar.chooseCosmetic(option.getInput());
      
      System.out.println("You have selected " + Avatar.cosmetics.getLast() + " for your character");
      
    }
    
    System.out.println("Your character has the following items :");
    
    PrintCosmetics.print();
  }
  
}
