package edu.pnw.softwaredesign.avatar;

/**
 * Creates the Male Avatar.
 * 
 * @author Piyush Kochhar
 *
 */
public class MaleAvatar extends Avatar {
  
  String decription;
  
  public String getDecription() {
    return decription;
  }
  
  public void setDecription(String decription) {
    this.decription = decription;
  }
  
  public MaleAvatar() {
    setDescription(description);
  }
}
