package edu.pnw.softwaredesign.avatar;

/**
 * Creates the Leather Shoes Cosmetic.
 * 
 * @author Piyush Kochhar
 *
 */
public class LeatherShoes extends AvatarDecorator {
  
  private String cosmetic = "Leather Shoes";
  
  private Avatar avatar;
  
  /**
   * Adds Leather Shoes to Avatar.
   * 
   * @param avatar Takes an Avatar Object
   */
  public LeatherShoes(Avatar avatar) {
    super();
    this.avatar = avatar;
    Avatar.cosmetics.add(cosmetic);
  }
  
  @Override
  public String getDescription() {
    return cosmetic;
  }
  
}
