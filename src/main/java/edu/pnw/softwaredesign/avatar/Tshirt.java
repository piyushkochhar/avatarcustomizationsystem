package edu.pnw.softwaredesign.avatar;

/**
 * Creates the T-Shirt cosmetic.
 * 
 * @author Piyush Kochhar
 *
 */
public class Tshirt extends AvatarDecorator {
  
  private String cosmetic = "T-shirt";
  
  private Avatar avatar;
  
  /**
   * Adds T-Shirt to Avatar.
   * 
   * @param avatar Takes an Avatar Object
   */
  
  public Tshirt(Avatar avatar) {
    super();
    this.avatar = avatar;
    Avatar.cosmetics.add(cosmetic);
  }
  
  @Override
  public String getDescription() {
    return cosmetic;
  }
  
}
