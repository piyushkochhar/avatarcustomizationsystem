package edu.pnw.softwaredesign.avatar;

/**
 * Adds Cosmetics to the Avatar.
 * 
 * @author Piyush Kochhar
 *
 */
public class AvatarWithCosmetics extends Avatar {
  
  Avatar avatar;
  
  /**
   * Applies cosmetic to the Avatars.
   * 
   * @param inputStr takes the input string
   */
  public void chooseCosmetic(String inputStr) {
    
    int input = Integer.parseInt(inputStr);
    
    if (input == 1) {
      avatar = new Jacket(avatar);
    } else if (input == 2) {
      avatar = new Tshirt(avatar);
    } else if (input == 3) {
      avatar = new Sweater(avatar);
    } else if (input == 4) {
      avatar = new Jeans(avatar);
    } else if (input == 5) {
      avatar = new Shorts(avatar);
    } else if (input == 6) {
      avatar = new Tie(avatar);
    } else if (input == 7) {
      avatar = new Necklace(avatar);
    } else if (input == 8) {
      avatar = new Earrings(avatar);
    } else if (input == 9) {
      avatar = new Sunglasses(avatar);
    } else if (input == 10) {
      avatar = new LeatherShoes(avatar);
    } else if (input == 11) {
      avatar = new HighHeels(avatar);
    } else if (input == 12) {
      avatar = new RunningShoes(avatar);
    }
    
  }
  
  /**
   * Refers to the current avatar.
   * 
   * @param avatar takes the avatar
   */
  public AvatarWithCosmetics(Avatar avatar) {
    this.avatar = avatar;
    
  }
  
  public Avatar getAvatar() {
    return avatar;
  }
  
}
