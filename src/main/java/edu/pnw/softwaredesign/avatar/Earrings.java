package edu.pnw.softwaredesign.avatar;

/**
 * Creates Earring cosmetic.
 * 
 * @author Piyush Kochhar
 *
 */
public class Earrings extends AvatarDecorator {
  
  private String cosmetic = "Earrings";
  
  private Avatar avatar;
  
  /**
   * Adds Earring to Avatar.
   * 
   * @param avatar Takes an Avatar Object
   */
  public Earrings(Avatar avatar) {
    super();
    this.avatar = avatar;
    Avatar.cosmetics.add(cosmetic);
  }
  
  @Override
  public String getDescription() {
    return cosmetic;
  }
  
}
