package edu.pnw.softwaredesign.avatar;

/**
 * Creates the Jeans Cosmetic.
 * 
 * @author Piyush Kochhar
 *
 */
public class Jeans extends AvatarDecorator {
  
  private String cosmetic = "Jeans";
  
  private Avatar avatar;
  
  /**
   * Adds Jeans to Avatar.
   * 
   * @param avatar Takes an Avatar Object
   */
  public Jeans(Avatar avatar) {
    super();
    this.avatar = avatar;
    Avatar.cosmetics.add(cosmetic);
  }
  
  @Override
  public String getDescription() {
    return cosmetic;
  }
  
}
