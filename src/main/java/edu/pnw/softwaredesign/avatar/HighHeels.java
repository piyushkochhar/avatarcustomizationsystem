package edu.pnw.softwaredesign.avatar;

/**
 * Creates the High Heels Cosmetic.
 * 
 * @author Piyush Kochhar
 *
 */
public class HighHeels extends AvatarDecorator {
  
  private String cosmetic = "High Heels";
  
  private Avatar avatar;
  
  /**
   * Adds High Heels to Avatar.
   * 
   * @param avatar Takes an Avatar Object
   */
  public HighHeels(Avatar avatar) {
    super();
    this.avatar = avatar;
    Avatar.cosmetics.add(cosmetic);
  }
  
  @Override
  public String getDescription() {
    return cosmetic;
  }
  
}
