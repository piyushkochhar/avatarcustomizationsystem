package edu.pnw.softwaredesign.avatar;

/**
 * Example Implementation of Avatar Customization System.
 * 
 * @author Piyush Kochhar
 *
 *
 */

public class Main {
  
  /**
   * Test class.
   * 
   * @param args command line arguments
   */
  public static void main(String[] args) {
    
    AvatarCustomizationSystem avatarSystem = new AvatarCustomizationSystem();
    
    avatarSystem.run();
    
  }
  
}
