package edu.pnw.softwaredesign.avatar;

/**
 * Creates the Sweater cosmetic.
 * 
 * @author Piyush Kochhar
 *
 */
public class Sweater extends AvatarDecorator {
  private String cosmetic = "Sweater";
  private Avatar avatar;
  
  /**
   * Adds Sweater to Avatar.
   * 
   * @param avatar Takes an Avatar Object
   */
  public Sweater(Avatar avatar) {
    super();
    this.avatar = avatar;
    Avatar.cosmetics.add(cosmetic);
  }
  
  @Override
  public String getDescription() {
    return cosmetic;
  }
  
}
