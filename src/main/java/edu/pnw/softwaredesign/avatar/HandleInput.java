package edu.pnw.softwaredesign.avatar;

/**
 * Handles the input from command line.
 * 
 * @author Piyush Kochhar
 *
 */
public interface HandleInput {
  
  String getInput();
}
