package edu.pnw.softwaredesign.avatar;

import java.util.Scanner;

/**
 * Handles the inputs from the command line.
 * 
 * @author Piyush Kochhar
 *
 */
public class UserInput implements HandleInput {
  
  Scanner sc = new Scanner(System.in);
  private String input = sc.nextLine();
  
  /**
   * Gets the user input.
   * 
   */
  public String getInput() {
    
    return input;
    
  }
  
  /**
   * Sets the user input.
   * 
   * @param number Takes a number as string
   * 
   */
  public void setInput(String number) {
    
    input = number;
    
  }
  
}
