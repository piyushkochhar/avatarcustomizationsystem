package edu.pnw.softwaredesign.avatar;

/**
 * Creates a Female Avatar.
 * 
 * @author Piyush Kochhar
 *
 */
public class FemaleAvatar extends Avatar {
  
  String decription;
  
  public String getDecription() {
    return decription;
  }
  
  public void setDecription(String decription) {
    this.decription = decription;
  }
  
  public FemaleAvatar() {
    setDescription(description);
  }
}
