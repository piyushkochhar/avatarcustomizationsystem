package edu.pnw.softwaredesign.avatar;

/**
 * Creates the Sunglasses cosmetic.
 * 
 * @author Piyush Kochhar
 *
 */
public class Sunglasses extends AvatarDecorator {
  
  private String cosmetic = "Sunglasses";
  
  private Avatar avatar;
  
  /**
   * Adds Sunglasses to Avatar.
   * 
   * @param avatar Takes an Avatar Object
   */
  public Sunglasses(Avatar avatar) {
    super();
    this.avatar = avatar;
    Avatar.cosmetics.add(cosmetic);
  }
  
  @Override
  public String getDescription() {
    return cosmetic;
  }
}
