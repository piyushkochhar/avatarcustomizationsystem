package edu.pnw.softwaredesign.avatar;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 * Applies Gender to the Avatar.
 * 
 * @author Piyush Kochhar
 *
 */
public class AvatarWithGender extends Avatar {
  
  Avatar avatar;
  
  ApplicationContext context = new FileSystemXmlApplicationContext("spring.xml");
  
  /**
   * Select Male or Female Avaatar.
   * 
   * @param inputStr Takes an input string
   */
  public AvatarWithGender(String inputStr) {
    
    int input = Integer.parseInt(inputStr);
    
    if (input == 1) {
      avatar = (Avatar) context.getBean("maleAvatar");
    } else if (input == 2) {
      avatar = (Avatar) context.getBean("femaleAvatar");
    }
  }
  
  public Avatar getAvatar() {
    return avatar;
  }
  
  
}
