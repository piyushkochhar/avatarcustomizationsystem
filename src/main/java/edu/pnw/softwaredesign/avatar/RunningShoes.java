package edu.pnw.softwaredesign.avatar;

/**
 * Creates tye Running Shoes cosmetic.
 * 
 * @author Piyush Kochhar
 *
 */
public class RunningShoes extends AvatarDecorator {
  
  private String cosmetic = "Running Shoes";
  
  private Avatar avatar;
  
  /**
   * Adds Running Shoes to Avatar.
   * 
   * @param avatar Takes an Avatar Object
   */
  public RunningShoes(Avatar avatar) {
    super();
    this.avatar = avatar;
    Avatar.cosmetics.add(cosmetic);
  }
  
  @Override
  public String getDescription() {
    return cosmetic;
  }
  
}
