package edu.pnw.softwaredesign.avatar;

/**
 * Creates the Jacket Cosmetic.
 * 
 * @author Piyush Kochhar
 *
 */
public class Jacket extends AvatarDecorator {
  
  private String cosmetic = "Jacket";
  
  private Avatar avatar;
  
  /**
   * Adds Jacket to Avatar.
   * 
   * @param avatar Takes an Avatar Object
   */
  public Jacket(Avatar avatar) {
    super();
    this.avatar = avatar;
    Avatar.cosmetics.add(cosmetic);
  }
  
  @Override
  public String getDescription() {
    return cosmetic;
  }
  
}
