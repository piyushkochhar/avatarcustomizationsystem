package edu.pnw.softwaredesign.avatar;

/**
 * Prints the selected cosmetics by Avatar.
 * 
 * @author Piyush Kochhar
 *
 */
public class PrintCosmetics {
  
  /**
   * Print selected cosmetics.
   */
  public static void print() {
    for (String i : Avatar.cosmetics) {
      System.out.print(i + ",");
    }
  }
}
